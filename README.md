# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

_Qué cosas necesitas para instalar el software y cómo instalarlas_

```
Para comenzar vamos a utilizar Ubuntu 20.0.4, puedes utilizar tanto una máquina virtual con esta versión o instalarlo de forma nativa en tu ordenador. A partir de ahi, continuaremos con la instalación del software necesario.

Primero debemos instalar Visual Studio Code, el programa en el que vamos a programar el código. Se puede hacer de muchas formas, aquí vamos a mostrar como hacerlo por consola de comandos.

Para ello abrimos una terminal con Ctrl+Alt+T y ponemos los siguientes comandos

$ sudo snap install --classic code
$ code .

Con el primer comando estariamos insatalando el Visual Studio code, y con el segundo lo lanzamos.

Hacemos lo mismo con Postman


$ sudo snap install postman
$ postman &

Ahora vamos a instalar y probar la ultima versión de Node.
Abrimos una terminal con Ctrl+Alt+T e instalamos el gestor de paquetes node:

$ sudo apt update
$ sudo apt install npm

A continuación, instalamos con npm una utilidad que ayuda a instalar y mantener las versiones de Node

$ sudo npm clean -f
$ sudo npm i -g n

Por último, instalamos la última versión estable de Node
$ sudo n stable

Para comprobar la versión podemos utilizar:

$ node --version
$ npm -v


Y podemos probar la consola node, que es un interprete de JS

$ node
> 3+4
7
> console.log("Hola a todos!");
Hola a todos!
undefined
> .exit [o Contorl+C para salir]
$

Instalamos también express, una biblioteca que crea una capa adicional sobre NodeJS que facilita enormemente la gestión de métodos y recursos HTTP

$ npm i -S express

Vamos a instalar nodemon, un gestor de proyectos que nos evita tener que estar reiniciando nuestra aplicación con cada cambio, ya que se ocupa de relanzarla cuando detecta algún cambio.

Instalamos Mongo de la siguiente forma:

$ cd
$ cd node
$ cd api-rest2
$ cd api-rest2 #este es el directorio en el que tengo yo el proyecto

$npm i -D nodemon

Y por último instalamos mongodb, el sistema de bases de datos.

$ sudo apt update
$ sudo apt install -y mongodb

Finalmente, en nuestro proyecto tendremos que instalar la biblioteca mongodb para trabajar con la base de datos y, en nuestro caso, la biblioteca mongojs que nos simplificará el acceso a mongo desde nuestro proyecto node

$ cd
$ cd node/api-rest
$ npm i -S mongodb
$ npm i -S mongojs

Ahora ya tenemos todo listo para lanzar nuestra aplicación.
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Indica cómo será ese paso_

```
Para tener operativa nuestra aplicación, debemos poner en marcha todos los componentes necesarios.

Necesitaremos 3 terminales distintas <Ctrl+Alt+T>
En la primera iniciaremos mongo 
$ sudo systemctl start mongodb

En la segunda lanzamos nuestra aplicación
$ cd ~/node/api-rest
$ npm start

Y en la tercera indicamos el puerto que va a escuchar mongo
$ mongo --host localhost:27017

Ya tenemos lanzada nuestra aplicación. Con esto, si ingresasemos en un navegador e introdujesemos http://localhost:3000/api/ , veriamos las colecciones que tenemos. En nuestro caso tenemos familia y perros

```




## Ejecutando las pruebas ⚙️

_Explica cómo ejecutar las pruebas automatizadas para este sistema_

Para probar la aplicación tenemos varias opciones.

Si queremos añadir algún elemento a alguna de las colecciones, vamos a Postman, y creamos una petición POST. 

Un ejemplo de peticion POST en Postman sería el siguiente:
Petición
    POST http://localhost:3000/api/familia
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {   "tipo": "Hermano",
        "nombre": "Pepe",
        "edad": 46
    }

Si queremos eliminar un objeto, sería hacer lo mismo que en el ejemplo anterior pero con una peticion DELETE.
Y si lo que queremos es añadir o modificar algo de un elemento ya existente, hariamos una peticion PUT.

Si queremos recuperar los datos de una colección en concreto, podríamos tanto realizar una petición GET a traves de postman, o introducir http://localhost:3000/api/(nombre de la colección) en el navegador.


## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Visual Studio Code](https://code.visualstudio.com/) - Editor de código usado.
* [Postman](https://www.postman.com/) - Herramienta para hacer llamadas al servidor web.
* [NodeJS](https://nodejs.org/es/) - Entorno de ejecución de JavaScript
* [MongoDB](https://www.mongodb.com/) - Sistema de bases de datos


## Versionado 📌

Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Javier José Botella Rico** - *Documentación e implementación de la práctica* - [fulanitodetal](#fulanito-de-tal)



## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.

