//API-REST parametrizable

'use strict'
const port = process.env.PORT || 3000;

const https = require('https');
const fs = require('fs');

/*OPTIONS HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('.cert/cert.pem')
};*/





const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();

const cors = require('cors');



// con esto enlazamos con la db
var db = mongojs("SD");
// Podriamos poner mas parametros, como por ejemplo: var db = mongojs('username:password@example.com/SD');
var id = mongojs.ObjectId;

// Declaraciones
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};
var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

// middlewares
var allowMethods = (req, res, next) => {
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    return next();
};
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "token");
    return next();
};
var auth = (req, res, next) => {
    if(req.headers.token === "password1234") {
        return next();
    } 
    else {
        return next(new Error("No autorizado"));
    };
};
// Declaramos middlewares

app.use(logger('dev'));  //probar con tiny, short, dev, common, combined (cositas de dev vamos)
app.use(express.urlencoded({ extended: false}));
app.use(express.json());
app.use(allowMethods);
app.use(allowCrossTokenHeader);
app.use(cors());


//funcion de callback para cuando introduzcamos colleciones nuevas
/*app.param("coleccion", (req, res, next, coleccion) => {
     console.log('param /api/:coleccion');
     console.log('colección: ', coleccion);
    
     req.collection = db.collection(coleccion);
     return next();
});*/


//metodos http, callback para cada metodo y ruta

/*app.metodo('ruta', (req, res, next) => {
    //Invocamos a la funcion de la biblioteca mongojs que corresponda
})*/

app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(db.user);
    
    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);
    });
});

app.get('/api/user', (req, res, next) => {
    db.user.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id', (req, res, next) => {
    db.user.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});


app.post('/api/:coleccion',  (req, res, next) => {
    const elemento = req.body;

    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } 
    else {
        db.user.save(elemento, (err, coleccionGuardada) => {
        if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/:coleccion/:id',  (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    
    db.user.update({_id: id(elementoId)},
    {$set: elementoNuevo}, {safe: true, multi: false}, (err, result) => {
        if (err) return next(err);
        res.json(result);
    });
});

app.delete('/api/:coleccion/:id',  (req, res, next) => {
    let elementoId = req.params.id;
    
    db.user.remove({_id: id(elementoId)}, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

//para hacer los metodos de api/auth
/*app.get('/api/auth', (req, res, next) => {
    db.user.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});*/

// Lanzamos nuestro servicio API
/*app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
});*/

https.createServer({
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
    }, app).listen(port, () => {
        console.log(`SEC WS API REST ejecutándose`);
    }
);

app.get('/foo', (rq, res) => {
console.log('hello i am foo');
});